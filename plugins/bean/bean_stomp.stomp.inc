<?php
/**
 * @file
 * Class which extends the BeanPlugin for the STOMP endpoint.
 */

class StompBean extends BeanPlugin {
  /**
   * Declares default block settings.
   */
  public function values() {
    return array(
      'body' => '',
    );
  }

  /**
   * Builds extra settings for the block edit form.
   */
  public function form($bean, $form, &$form_state) {
    $form = array();

    $form['body'] = array(
      '#type' => 'textfield',
      '#title' => t('Body'),
      '#default_value' => $bean->body,
      '#description' => t('Some extra text to go along with Hello World.'),
    );

    return $form;
  }

  /**
   * Displays the bean.
   */
  public function view($bean, $content, $view_mode = 'default', $langcode = NULL) {

    // Set up the configuration created in the admin form.
    $js_config = array(
      'stomp_host' => variable_get('stomp_host'),
      'stomp_port' => variable_get('stomp_port'),
      'stomp_user' => variable_get('stomp_user'),
      'stomp_pass' => variable_get('stomp_pass'),
      'stomp_queue' => variable_get('stomp_queue'),
    );

    $content['#markup'] = '<div class="stomp-wrapper"><div class="mq-messages"></div></div>';
    $content['#markup'] .= check_plain($bean->body);
    $content['#attached'] = array(
      'js' => array(
        drupal_add_js(array('beanStomp' => $js_config), 'setting'),
        drupal_add_js(drupal_get_path('module', 'bean_stomp') . '/js/stomp.js'),
        drupal_add_js(drupal_get_path('module', 'bean_stomp') . '/js/feed.js'),
      ),
    );

    return $content;
  }
}
