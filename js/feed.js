/**
 * @file
 * File that listens to the STOMP endpoint using WebSockets
 * This implementation only listens to endpoint to present a feed to the screen
 */

(function($){
  Drupal.behaviors.exampleModule = {
    attach: function (context, settings) {
    var supported = ("WebSocket" in window);
    if(!supported) {
      var msg = "Your browser does not support Web Sockets. This example will not work properly.<br>";
    	  msg += "Please use a Web Browser with Web Sockets support (WebKit or Google Chrome).";
    	  $(".stomp-wrapper").html(msg);
    }
    var client, destination;

    var url = 'ws://'+Drupal.settings.beanStomp.stomp_host+':'+Drupal.settings.beanStomp.stomp_port; //@todo change these hardocded to reading Drupal
    var login = Drupal.settings.beanStomp.stomp_user;
    var passcode = Drupal.settings.beanStomp.stomp_pass;
    destination = Drupal.settings.beanStomp.stomp_queue;

    client = Stomp.client(url);

    // this allows to display debug logs directly on the web page
    client.debug = function(str) {
      $("#debug").append(str + "\n");
    };
    // the client is notified when it is connected to the server.
    var onconnect = function(frame) {
      client.debug("connected to Stomp");
      //@todo - make this more streamlike and fluid
      client.subscribe(destination, function(message) {
    	  $(".mq-messages").replaceWith("<div class=\"mq-messages\"><p>"+ message.body + "</p><div>");
      });	  
    };
    client.connect(login, passcode, onconnect);

    return false;
	}
  };
})(jQuery);