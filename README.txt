This module provides an implementation of Jeff Mesnil's STOMP over WebSockets
JavaScript library for Drupal. 

As per the website, STOMP over WebSocket's provides a limited version of STOMP.

This module allows the user to push feeds from a STOMP overWebSockets endpoint
to a web page. For instance results of an experiment or the classic stock ticker. 

The PHP uses the BEAN project to provide a block. 

The feed.js file actually deals with the message and printing it out to screen. 

===Installation====

You will need to download the stomp.js library from the Jeff Mesnil's Github:
https://raw.github.com/jmesnil/stomp-websocket/master/dist/stomp.min.js

Install this into the Bean_stomp module's /js directory as stomp.js

