<?php
/**
 * @file
 * Admin form functions for Bean Stomp module
 */

/**
 * 
 * Implements hook_form
 * Store the details of the public endpoint
 * @param string $form
 * @param object $form_state
 */
function bean_stomp_admin_form($form, &$form_state) {
  $form['stomp'] = array(
    '#type' => 'fieldset', 
    '#title' => 'Stomp settings',
    '#description' => 'Store the setting for the public STOMP over WebSockets endpoint',
  );
  $form['stomp']['host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#default_value' => variable_get('stomp_host'),
    '#description' => t('Host name where the STOMP over WebSockets endpoint'),
      '#size' => 15,
      '#maxlength' => 20,
      '#required' => TRUE,
  );
  
  $form['stomp']['port'] = array(
      '#type' => 'textfield',
      '#title' => t('Port'),
      '#default_value' => variable_get('stomp_port'),
      '#description' => t('Port to listen on'),
        '#size' => 8,
        '#maxlength' => 10,
        '#required' => TRUE,
  );
  $form['stomp']['user'] = array(
      '#type' => 'textfield',
      '#title' => t('User Name'),
      '#default_value' => variable_get('stomp_user'),
      '#description' => t('User name for the STOMP over WebSockets endpoint'),
        '#size' => 15,
        '#maxlength' => 20,
        '#required' => TRUE,
  );
  $form['stomp']['pass'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#default_value' => variable_get('stomp_pass'),
    '#description' => t('Password for the STOMP over WebSockets endpoint is.'),
      '#size' => 15,
      '#maxlength' => 20,
      '#required' => TRUE,
  );
  $form['stomp']['queue'] = array(
    '#type' => 'textfield',
    '#title' => t('The queue or topic to listen to'),
    '#default_value' => variable_get('stomp_queue'),
    '#description' => t('Must start with /topic/ or /queue/'),
      '#size' => 20,
      '#maxlength' => 150,
      '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save settings'),
  );
  return $form;
}

/**
 * Implements hook_form_validate()
 */
function bean_stomp_admin_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  
  $queue = explode('/', $values['queue']);
  if ($queue[1] != 'queue' && $queue[1] != 'topic') {
    form_set_error('Bean Stomp', 'The STOMP endpoint must have a /queue/ or /topic/ at the start');
  }
}

/**
* Implements hook_form_submit()
* Function to store the form values as variables in the cache
*/
function bean_stomp_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  
  variable_set('stomp_host', $values['host']);
  variable_set('stomp_port', $values['port']);
  variable_set('stomp_user', $values['user']);
  variable_set('stomp_pass', $values['pass']);
  variable_set('stomp_queue', $values['queue']);
}